using System.Collections.Generic;


namespace Valhalla.UltimateLocalization
{
	public static class WrongCharactersHandler
	{
		private static readonly List<(string oldValue, string newValue)> Replaces
			= new()
			{
				("\r\n", "\n"),
				("\r", "\n"),
			};
		
		
		public static string HandleWrongCharacters(string s)
		{
			if (string.IsNullOrEmpty(s))
				return "";
			
			foreach (var (oldValue, newValue) in Replaces)
				s = s.Replace(oldValue, newValue);

			return s;
		}
	}
}
