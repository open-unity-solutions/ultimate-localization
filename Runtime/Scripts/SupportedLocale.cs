namespace Valhalla.UltimateLocalization
{
	public enum SupportedLocale
	{
		En,
		Ru,
		Cn,
	}
}
