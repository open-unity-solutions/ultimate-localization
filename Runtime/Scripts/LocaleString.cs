﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using LabelTextAttribute = Sirenix.OdinInspector.LabelTextAttribute;
#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;
#endif

#if SGP_ASYNC
using ScriptGraphPro.Attributes.Fields;
using Newtonsoft.Json;
#endif

#if !LOCALE_RU && !LOCALE_EN && !LOCALE_CN
#error At least one locale needed
#endif

#if !LOCALE_RU_REQUIRED && !LOCALE_EN_REQUIRED && !LOCALE_CN_REQUIRED
#error At least one locale must be required
#endif


namespace Valhalla.UltimateLocalization
{
	[Serializable]
	public class LocaleString : ISelfValidator
	{
		private const int LABEL_WIDTH = 50;
		
		public static SupportedLocale CurrentLocale = SupportedLocale.En;

		public static List<LocaleString> CachedNewStrings = new();

		public static UnityEvent<LocaleString> OnDeserializedEvent = new();
		

		// Public for AOT (IL2CPP)
		[SerializeField]
		public string _id;


				
		#if SGP_ASYNC
		[ScriptGraphPro.Attributes.Fields.Searchable]
		[JsonProperty(Required = Required.DisallowNull)]
		
		#if LOCALE_EN
		[NodeContent("En")]
		#endif
		#endif
		
		#if LOCALE_EN
		[LabelWidth(LABEL_WIDTH)]
		#else
		[HideInInspector]
		#endif
		
		public string En;
		
		
		
		#if SGP_ASYNC
		[ScriptGraphPro.Attributes.Fields.Searchable]
		[JsonProperty(Required = Required.DisallowNull)]
		
		
		#if LOCALE_RU
		[NodeContent("Ru")]
		#endif
		#endif
		
		#if LOCALE_RU
		[LabelWidth(LABEL_WIDTH)]
		#else
		[HideInInspector]
		#endif
		
		public string Ru;

		
		
		#if SGP_ASYNC
		[ScriptGraphPro.Attributes.Fields.Searchable]
		[JsonProperty(Required = Required.DisallowNull)]
		
		#if LOCALE_CN
		[NodeContent("Cn")]
		#endif
		#endif
		
		#if LOCALE_CN
		[LabelWidth(LABEL_WIDTH)]
		#else
		[HideInInspector]
		#endif
		
		public string Cn;

		
		
		#if UNITY_EDITOR
		#if SGP_ASYNC
		[JsonIgnore]
		#endif
		public static readonly List<string> LocaleFiledNames = new List<string>()
		{
			nameof(Ru),
			nameof(En),
			nameof(Cn),
		};
		#endif
		
		
		#if SGP_ASYNC
		[JsonIgnore]
		#endif
		public Guid ID
		{
			get => Guid.Parse(_id);  // TODO cache

			set
			{
				if (value == default)
				{
					value = Guid.NewGuid();
					CachedNewStrings.Add(this);
				}

				_id = value.ToString();
			}
		}
		
		
		#if SGP_ASYNC
		[JsonIgnore]
		#endif
		public bool IsEmpty
			=> string.IsNullOrEmpty(Value);

		
		#if SGP_ASYNC
		[JsonIgnore]
		#endif
		public string Value
			=> this[CurrentLocale];
		
		
		public string this[SupportedLocale locale]
		{
			get => locale switch
			{
				SupportedLocale.Ru => WrongCharactersHandler.HandleWrongCharacters(Ru),
				SupportedLocale.En => WrongCharactersHandler.HandleWrongCharacters(En),
				SupportedLocale.Cn => WrongCharactersHandler.HandleWrongCharacters(Cn),
				_ => throw new ArgumentOutOfRangeException(),
			};

			set
			{
				switch (locale)
				{
					case SupportedLocale.Ru:
						Ru = value ?? "";
						break;

					case SupportedLocale.En:
						En = value ?? "";;
						break;
					
					case SupportedLocale.Cn:
						Cn = value ?? "";;
						break;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public LocaleString() : this("", "", "", default)
		{
			
		}

		
		#if SGP_ASYNC
		[JsonConstructor]
		#endif
		public LocaleString(string ru, string en, string cn, Guid id)
		{
			Ru = ru ?? "";
			En = en ?? "";
			Cn = cn ?? "";

			ID = id;
		}
		
		
		public string RemovePatterns(SupportedLocale lang, params string[] patterns)
		{
			string result = this[lang];
			
			foreach (var pattern in patterns)
			{
				var matches = Regex.Matches(result, pattern);

				foreach (Match match in matches)
					result = result.Replace(match.Groups[0].Value, "");
			}

			return result;
		}


		[OnDeserialized]
		private void OnDeserializedEventHandler(StreamingContext context)
		{
			OnDeserializedEvent.Invoke(this);
		}

		public override string ToString()
			=> Value;


		public void Validate(SelfValidationResult result)
		{
			var emptyFields = new List<string>();
			
			#if LOCALE_RU && LOCALE_RU_REQUIRED
			if (string.IsNullOrEmpty(Ru))
				emptyFields.Add(nameof(Ru));
			#endif
			
			
			#if LOCALE_EN && LOCALE_EN_REQUIRED
			if (string.IsNullOrEmpty(En))
				emptyFields.Add(nameof(En));
			#endif
			
			#if LOCALE_CN && LOCALE_CN_REQUIRED
			if (string.IsNullOrEmpty(Cn))
				emptyFields.Add(nameof(Cn));
			#endif
			
			
			if (emptyFields.Count > 0)
			{
				var locales = String.Join(", ", emptyFields);
				result.AddError($"Values required: {locales}");
			}
		}
	}
	
	#if UNITY_EDITOR


	public class LocaleStringAttributeProcessor : OdinAttributeProcessor<LocaleString>
	{
		private bool _isMultiline = false;


		public override void ProcessSelfAttributes(InspectorProperty property, List<Attribute> attributes)
		{
			base.ProcessSelfAttributes(property, attributes);

			_isMultiline = attributes.RemoveAttributeOfType<MultilineAttribute>();
			_isMultiline |= attributes.RemoveAttributeOfType<MultiLinePropertyAttribute>();

			if (attributes.Any(attr => attr is LabelTextAttribute))
				return;
			
			if (!_isMultiline)
				attributes.Add(new LabelTextAttribute(GetTitle(property)));
		}


		private string GetTitle(InspectorProperty property)
		{
			var value = (LocaleString) property.ValueEntry.WeakSmartValue;
			
			if (value == null)
				return "";
				
			var content = value.Value;
			

			if (string.IsNullOrEmpty(content))
				content = $"!Empty value on {LocaleString.CurrentLocale}!";

			var name = property.Name;
			if (name.StartsWith("_"))
				name = name.Substring(1);
			
			name = name.Replace("_", " ");
			
			name = char.ToUpper(name[0]) + name.Substring(1);

			if (name[0] == '$')
				name = $" {name}";
			
			return $"{name} ({content})";
		}


		public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member, List<Attribute> attributes)
		{
			base.ProcessChildMemberAttributes(parentProperty, member, attributes);
			
			bool isLocaleLine = LocaleString.LocaleFiledNames.Contains(member.Name);
			
			if (isLocaleLine && _isMultiline)
				attributes.Add(new MultiLinePropertyAttribute(5));
		}
	}


	#endif
}
