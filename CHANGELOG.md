## [0.1.5]

### Fixed
- Multiline property rendering
- CI/CD updated


## [0.1.4]

### Fixed
- Underscore name handling of `LocaleString`
- Collection entries name handling of `LocaleString`

### Changed
- Licence year updated


## [0.1.3]

### Added
- Current language value is now displayed as title in inspector


## [0.1.2]

### Changed
- Access granted for AOT hacks


## [0.1.1]

### Fixed
- Correct displaying lines in Loki 


## [0.1.0]

### Added
- Defines `LOCALE_EN_REQUIRED`, `LOCALE_RU_REQUIRED`, `LOCALE_CN_REQUIRED`

### Changed
- Rebranded to Valhalla
- Default language is EN now

### Removed
- Ignore translation and placeholder settings in `LocaleString`


## [0.0.2]

### Added 
- Defines `LOCALE_RU`, `LOCALE_EN`, `LOCALE_CN`


## [0.0.1]

### Added
- Localizable strings for SO and other data
